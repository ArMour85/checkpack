# Package

version       = "0.3.0"
author        = "Arnaud Moura"
description   = "Tiny library to check if a system package is already installed."
license       = "MIT"
srcDir        = "src"


# Dependencies

requires "nim >= 1.2.0"
