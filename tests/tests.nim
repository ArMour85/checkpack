import unittest

import checkpack, re, strutils

test "Check cmake":
    check checkPack("cmake") == true

test "Check lol":
    check checkPack("lol") == false

test """Check re"^cmake.*""":
    check checkPack(re"^cmake.*") == true

test """Check re"^lol.*""":
    check checkPack(re"^lol.*") == false

test "Check list with cmake and lol":
    check checkPacks(@["lol", "cmake"]) == @["lol"]

test "Check list available package manager":
    when defined(windows):
        check getAvailablePackageManager() == @[choco]
    elif defined(linux):
        for line in readFile("/etc/os-release").split("\n"):
            if line.startsWith("ID="):
                let osname = line[3..^1].strip(chars={'"', '\''})
                if osname == "ubuntu":
                    check getAvailablePackageManager() == @[dpkg, apt]
    elif defined(macosx):
        check getAvailablePackageManager() == @[brew]